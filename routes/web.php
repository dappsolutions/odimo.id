<?php

use Illuminate\View\View;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//root
Route::get('/', function () {
    // echo 'ODIMO.ID';
    return view("login.login");
});
//root

//dashboard
Route::get('dashboard', 'Dashboard\DashboardController@index');

//guru
Route::get('pembelian', 'Pembelian\PembelianController@index');
Route::post('pembelian/simpan', 'Pembelian\PembelianController@simpanTransksi');
Route::get('pembelian/getData', 'Pembelian\PembelianController@getDataProduk');
Route::get('pembelian/getDataForAdmin', 'Pembelian\PembelianController@getDataProdukForAdmin');


//pengeluaran
Route::get('pengeluaran', 'Pembelian\PengeluaranController@index');
Route::post('pengeluaran/simpan', 'Pembelian\PengeluaranController@simpanTransksi');
Route::get('pengeluaran/getData', 'Pembelian\PengeluaranController@getDataProduk');


//product stock
Route::get('product_stok', 'Pembelian\ProdukStokController@index');
Route::get('product_stok/getData', 'Pembelian\ProdukStokController@getDataProdukStok');
Route::get('product_stok/getDataForAdmin', 'Pembelian\ProdukStokController@getDataProdukStokAdmin');
Route::get('product_stok/getDataProdukResellerByNamaProduct/{id}', 'Pembelian\ProdukStokController@getDataProdukResellerByNamaProduct');
Route::post('product_stok/simpan', 'Pembelian\ProdukStokController@simpanTransksi');
Route::post('product_stok/simpanStok', 'Pembelian\ProdukStokController@simpanStok');

// Route::get('pembelian/getData', 'Pembelian\PembelianController@getDataProduk');


Route::get('login/sign_out', 'Login\LoginController@sign_out');

//login ajax
Route::post('login/sign_in', 'Login\LoginController@sign_in');


//produk admin
Route::get('data/produk_reseller', 'Data\ProdukResellerController@index');
Route::get('data/produk_reseller/cari', 'Data\ProdukResellerController@cari');
Route::get('data/produk_reseller/add', 'Data\ProdukResellerController@add');
Route::get('data/produk_reseller/detail/{id}', 'Data\ProdukResellerController@detail');
Route::get('data/produk_reseller/edit/{id}', 'Data\ProdukResellerController@edit');

Route::post('data/produk_reseller/editStok', 'Data\ProdukResellerController@editStok');
Route::post('data/produk_reseller/keepStok', 'Data\ProdukResellerController@keepStok');
Route::post('data/produk_reseller/simpanStok', 'Data\ProdukResellerController@simpanStok');
Route::post('data/produk_reseller/prosesKeepStok', 'Data\ProdukResellerController@prosesKeepStok');
Route::post('data/produk_reseller/submit', 'Data\ProdukResellerController@submit');


//produk reseller
Route::get('data/stok_reseller', 'Data\StokResellerController@index');
Route::get('data/stok_reseller/cari', 'Data\StokResellerController@cari');
Route::get('data/stok_reseller/add', 'Data\StokResellerController@add');
Route::get('data/stok_reseller/detail/{id}', 'Data\StokResellerController@detail');
Route::get('data/stok_reseller/edit/{id}', 'Data\StokResellerController@edit');

Route::post('data/stok_reseller/submit', 'Data\StokResellerController@submit');

//produk keep
Route::get('data/stok_keep', 'Data\StokKeepController@index');
Route::get('data/stok_keep/cari', 'Data\StokKeepController@cari');
Route::get('data/stok_keep/add', 'Data\StokKeepController@add');
Route::get('data/stok_keep/detail/{id}', 'Data\StokKeepController@detail');
Route::get('data/stok_keep/edit/{id}', 'Data\StokKeepController@edit');

Route::post('data/stok_keep/cancelKeep', 'Data\StokKeepController@cancelKeep');
Route::post('data/stok_keep/confirmKeep', 'Data\StokKeepController@confirmKeep');

//pembelian
Route::get('data/pembelian', 'Data\PembelianController@index');
Route::get('data/pembelian/cari', 'Data\PembelianController@cari');
Route::get('data/pembelian/add', 'Data\PembelianController@add');
Route::get('data/pembelian/detail/{id}', 'Data\PembelianController@detail');
Route::get('data/pembelian/edit/{id}', 'Data\PembelianController@edit');

//pengeluaran
Route::get('data/pengeluaran', 'Data\PengeluaranController@index');
Route::get('data/pengeluaran/cari', 'Data\PengeluaranController@cari');
Route::get('data/pengeluaran/add', 'Data\PengeluaranController@add');
Route::get('data/pengeluaran/detail/{id}', 'Data\PengeluaranController@detail');
Route::get('data/pengeluaran/edit/{id}', 'Data\PengeluaranController@edit');

//pengeluaran myoffice
Route::get('myoffice/pengeluaran', 'Myoffice\PengeluaranController@index');
Route::get('myoffice/pengeluaran/cari', 'Myoffice\PengeluaranController@cari');
Route::get('myoffice/pengeluaran/add', 'Myoffice\PengeluaranController@add');
Route::get('myoffice/pengeluaran/detail/{id}', 'Myoffice\PengeluaranController@detail');
Route::get('myoffice/pengeluaran/edit/{id}', 'Myoffice\PengeluaranController@edit');

Route::post('myoffice/pengeluaran/submit', 'Myoffice\PengeluaranController@submit');

//pemasukan
Route::get('data/pemasukan', 'Data\PemasukanController@index');
Route::get('data/pemasukan/cari', 'Data\PemasukanController@cari');
Route::get('data/pemasukan/add', 'Data\PemasukanController@add');
Route::get('data/pemasukan/detail/{id}', 'Data\PemasukanController@detail');
Route::get('data/pemasukan/edit/{id}', 'Data\PemasukanController@edit');
