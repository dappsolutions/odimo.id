<div class="row">
    <div class="col-md-12">
        <h5>Keep Stok</h5>
        <input type="hidden" id="produk-stok-id" value="{{ $id }}" class="form-control">
        <input type="hidden" id="sisa-stok" value="{{ $stok_sisa }}" class="form-control">

        <input type="text" class="form-control required" error="Customer" id="nama-customer" placeholder="Customer">
        <br>
        <textarea class="form-control" error="Alamat" id="deskripsi" placeholder="Alamat"></textarea>
        <div class="text-right">
            <a href="" class="" onclick="ProdukReseller.prosesKeepStok(this, event)">
                <i class="fa fa-check my-float fa-lg"></i>
            </a>
        </div>
    </div>
</div>
