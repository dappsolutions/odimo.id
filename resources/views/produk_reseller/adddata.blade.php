<input type="hidden" id='id' value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">{{ 'TAMBAH '.strtoupper($module) }}</div>

            <div class="card-body">
                <div class="form-layout">
                    <div class="row mg-b-25">

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Nama Produk : <span class="tx-danger">*</span></label>
                                <input id='nama_product' class="form-control required" error="Nama Produk" value="{{ isset($kategori) ? $kategori : '' }}" type="text" placeholder="Nama Produk">
                            </div>
                        </div><!-- col-4 -->

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Deskripsi Produk : <span class="tx-danger">*</span></label>
                                <input id='deskripsi' class="form-control required" error="Deskripsi Produk" value="{{ isset($kategori) ? $kategori : '' }}" type="text" placeholder="Deskripsi Produk">
                            </div>
                        </div><!-- col-4 -->

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Stok Produk : <span class="tx-danger">*</span></label>
                                <input id='stok' class="form-control required" error="Stok Produk" value="{{ isset($kategori) ? $kategori : '' }}" type="text" placeholder="Stok Produk">
                            </div>
                        </div><!-- col-4 -->

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Kategori Produk : <span class="tx-danger">*</span></label>
                                <select class="form-control" id="kategori">
                                    <option value="TALI">TALI</option>
                                    <option value="YUKENSI">YUKENSI</option>
                                    <option value="KANCING DEPAN">KANCING DEPAN</option>
                                </select>
                            </div>
                        </div><!-- col-4 -->

                        <div class="col-lg-4">
                            <div class="form-group">
                                <br/>
                                <button class="btn btn-primary" onclick="ProdukReseller.inputFile(this)">Input Gambar</button>
                            </div>
                        </div><!-- col-4 -->
                    </div><!-- row -->

                    <div class="row">
                        <div class="col-md-6">
						    <div class="images">
							    <div class="pic">
								    <img src="" id="source-doc" width="200" height="200"/>
							    </div>
						    </div>
                        </div>
                    </div>

                    <div class="form-layout-footer">
                        <div class="text-right">
                            <button class="btn btn-success mg-r-5" onclick="ProdukReseller.submit()">Submit Form</button>
                            <button class="btn btn-secondary" onclick="ProdukReseller.cancel()">Batal</button>
                        </div>
                    </div><!-- form-layout-footer -->
                </div>
            </div>

        </div>
    </div>
</div>
<br>

<div class="row">
    <div class="col-md-12">

    </div>
</div>
