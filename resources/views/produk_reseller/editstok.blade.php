<div class="row">
    <div class="col-md-12">
        <h5>Ubah Stok</h5>
        <input type="hidden" id="produk-stok-id" value="{{ $id }}" class="form-control">

        <input type="text" class="form-control required" error="Stok" id="stok" placeholder="Stok">
        <div class="text-right">
            <a href="" class="" onclick="ProdukReseller.prosesStok(this, event)">
                <i class="fa fa-check my-float fa-lg"></i>
            </a>
        </div>
    </div>
</div>
