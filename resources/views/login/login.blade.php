<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="ODIMMO.ID">
    <meta name="twitter:description" content="Aplikasi Manajemen Toko ODIMO.ID.">
    {{-- <meta name="twitter:image" content="http://themepixels.me/shamcey/img/shamcey-social.png"> --}}

    <!-- Facebook -->
    {{-- <meta property="og:url" content="http://themepixels.me/shamcey"> --}}
    <meta property="og:title" content="ODIMMO.ID">
    <meta property="og:description" content="Aplikasi Manajemen Toko ODIMO.ID.">

    {{-- <meta property="og:image" content="http://themepixels.me/shamcey/img/shamcey-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/shamcey/img/shamcey-social.png"> --}}
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Aplikasi Manajemen Toko ODIMO.ID.">
    <meta name="author" content="ThemePixels">


    <title>ODIMO.ID</title>

    <!-- Vendor css -->
    <link href="{{ asset('assets/lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">

    <!-- Shamcey CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/shamcey.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/owner.css') }}">
    <link href="{{ asset('assets/lib/SpinKit/spinkit.css') }}" rel="stylesheet">
  </head>

  <body class="bg-gray-900">
    <div class="signpanel-wrapper">
      <div class="signbox">
        <div class="signbox-header">
          <h2>ODIMO.ID</h2>
          <p class="mg-b-0">Aplikasi Management Toko</p>
        </div><!-- signbox-header -->
        <div class="signbox-body">
          <div class="loading hide">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
                <div class="text-center">
                    <p id='pesan-loading'></p>
                </div>
          </div>

          @if (session('message') != '')
                <div class="session_message">
                    <div class="alert alert-danger mg-b-0" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                        <strong class="d-block d-sm-inline-block-force">Sorry !!</strong> {{ session('message') }}
                    </div>
                </div>
                <br>
          @endif

          <div class='content-module'>
              <form action="" method="POST">
                  <div class="form-group">
                    <label class="form-control-label">Username :</label>
                    <input type="text" name="username" placeholder="Username" id='username' class="form-control required" error='Username'>
                  </div><!-- form-group -->
                  <div class="form-group">
                    <label class="form-control-label">Password :</label>
                    <input type="password" name="password" placeholder="Password" id='password' class="form-control required" error='Password'>
                  </div><!-- form-group -->
                  {{-- <div class="form-group">
                    <a href="">Forgot password?</a>
                  </div><!-- form-group --> --}}
                  <button class="btn btn-success btn-block" onclick="Login.sign_in(this, event)">Masuk</button>
                  <div class="tx-center bg-white bd pd-10 mg-t-40">Belum punya akun ? <a href="{{ url('registrasi') }}">Daftar Sebagai Member</a></div>
              </form>
          </div>
        </div><!-- signbox-body -->
      </div><!-- signbox -->
    </div><!-- signpanel-wrapper -->

    <script src="{{ asset('assets/lib/jquery/jquery.js') }}"></script>
    <script src="{{ asset('assets/lib/popper.js/popper.js') }}"></script>
    <script src="{{ asset('assets/lib/bootstrap/bootstrap.js') }}"></script>

    {{-- <script src="{{ asset('assets/js/shamcey.js') }}"></script> --}}
    <script src="{{ asset('assets/js/url.js') }}"></script>
    <script src="{{ asset('assets/js/toastr.min.js') }}"></script>
    <script src="{{ asset('assets/js/message.js') }}"></script>
    <script src="{{ asset('assets/js/validation.js') }}"></script>
    <script src="{{ asset('assets/js/controllers/login.js') }}"></script>
  </body>
</html>
