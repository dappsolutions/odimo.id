<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Twitter -->
        <meta name="twitter:site" content="@themepixels">
        <meta name="twitter:creator" content="@themepixels">
        <meta name="csrf_token" content="{{ csrf_token() }}" />
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="ODIMMO.ID">
        <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
        <!--<meta name="twitter:image" content="http://themepixels.me/shamcey/img/shamcey-social.png">-->

        <!-- Facebook -->
        <!--<meta property="og:url" content="http://themepixels.me/shamcey">-->
        <meta property="og:title" content="ODIMMO.ID">
        <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

       <!-- <meta property="og:image" content="http://themepixels.me/shamcey/img/shamcey-social.png">
        <meta property="og:image:secure_url" content="http://themepixels.me/shamcey/img/shamcey-social.png">-->
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1200">
        <meta property="og:image:height" content="600">

        <!-- Meta -->
        <meta name="description" content="Aplikasi Manajemen Toko ODIMO.ID.">
        <meta name="author" content="ThemePixels">


        <title>{{$title_top}}</title>

        <!-- Vendor css -->
       <link href="{{ asset('assets/lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
       <link href="{{ asset('assets/lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">
       <link href="{{ asset('assets/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
       <link href="{{ asset('assets/lib/SpinKit/spinkit.css') }}" rel="stylesheet">

        <!-- Shamcey CSS -->
       <link rel="stylesheet" href="{{ asset('assets/lib/jquery.steps/jquery.steps.css') }}">
       <link rel="stylesheet" href="{{ asset('assets/lib/select2/css/select2.min.css') }}">
       <link rel="stylesheet" href="{{ asset('assets/css/shamcey.css') }}">
       <link rel="stylesheet" href="{{ asset('assets/css/toastr.min.css') }}">
       <link rel="stylesheet" href="{{ asset('assets/css/owner.css') }}">
       <script src="{{ asset('assets/lib/jquery/jquery.js') }}"></script>
       <script src="{{ asset('assets/lib/popper.js/popper.js') }}"></script>
       <script src="{{ asset('assets/lib/bootstrap/bootstrap.js') }}"></script>
       <script src="{{ asset('assets/lib/jquery-ui/jquery-ui.js') }}"></script>
       <script src="{{ asset('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
       <script src="{{ asset('assets/lib/moment/moment.js') }}"></script>
       <script src="{{ asset('assets/lib/Flot/jquery.flot.js') }}"></script>
       <script src="{{ asset('assets/lib/Flot/jquery.flot.resize.js') }}"></script>
       <script src="{{ asset('assets/lib/flot-spline/jquery.flot.spline.js') }}"></script>
       <script src="{{ asset('assets/js/shamcey.js') }}"></script>
       <script src="{{ asset('assets/js/toastr.min.js') }}"></script>
       <script src="{{ asset('assets/js/bootbox.js') }}"></script>
       <script src="{{ asset('assets/js/controllers/template.js') }}"></script>

        @if (isset($header_data))
            @foreach ($header_data as $key => $v_head)
                @php
                    $data_key = explode('-', $key);
                @endphp
                @if ($data_key[0] == 'css')
                    <link rel="stylesheet" href="{{ $v_head }}">
                @else
                    <script src="{{ $v_head }}"></script>
                @endif
            @endforeach
        @endif
    </head>
    <body>
        @extends('template.navbar')

        {!! view('template.sidebar') !!}

        @if (session('access') === 'sekolah')
            {!! view('template.sidebaraccesssekolah') !!}
        @endif

        @if (session('access') === 'siswa')
            {!! view('template.sidebaraccesssiswa') !!}
        @endif


        @extends('template.navbarsecond')

        <div class="sh-mainpanel">
            <div class="sh-breadcrumb">
                <nav class="breadcrumb">
                    <a class="breadcrumb-item" href="#">{{ isset($module) ? ucfirst($module) : '' }}</a>
                    <span class="breadcrumb-item active">{{ isset($title_content) ? $title_content : '' }}</span>
                </nav>
            </div><!-- sh-breadcrumb -->
            <div class="sh-pagetitle">
                <div class="input-group">
                    <input type="search" class="form-control hide" placeholder="Search">
                    <span class="input-group-btn hide">
                        <button class="btn"><i class="fa fa-search"></i></button>
                    </span>
                </div>
                <div class="sh-pagetitle-left">
                    <div class="sh-pagetitle-icon"><i class="icon ion-ios-home"></i></div>
                    <div class="sh-pagetitle-title">
                        {{-- <span>All Features Summary</span> --}}
                        <h2>{{ isset($title_content) ? $title_content : '' }}</h2>
                    </div>
                </div>
            </div>
            <div class="sh-pagebody">
                @php
                    $hide_loading = isset($view_file) ? 'hide' : '';
                @endphp
                <div class="loading {{ $hide_loading }}">
                    <div class="sk-circle">
                        <div class="sk-circle1 sk-child"></div>
                        <div class="sk-circle2 sk-child"></div>
                        <div class="sk-circle3 sk-child"></div>
                        <div class="sk-circle4 sk-child"></div>
                        <div class="sk-circle5 sk-child"></div>
                        <div class="sk-circle6 sk-child"></div>
                        <div class="sk-circle7 sk-child"></div>
                        <div class="sk-circle8 sk-child"></div>
                        <div class="sk-circle9 sk-child"></div>
                        <div class="sk-circle10 sk-child"></div>
                        <div class="sk-circle11 sk-child"></div>
                        <div class="sk-circle12 sk-child"></div>
                    </div>
                    <div class="text-center">
                        <p id='pesan-loading'></p>
                    </div>
                </div>



                <div class="content-module">
                    {!! isset($view_file) ? $view_file : '' !!}
                </div>
            </div>

            <div class="sh-footer">
                <div>Copyright &copy; {{ date('Y') }}. All Rights Reserved</div>
                <div class="mg-t-10 mg-md-t-0">Designed by: <a href="https://solutionsdapps.com">Dappsolutions</a></div>
            </div><!-- sh-footer -->
        </div>
    </body>
</html>
