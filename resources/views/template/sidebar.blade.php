<div class="sh-sideleft-menu">
    <label class="sh-sidebar-label">Navigation</label>
    <ul class="nav">
     <li class="nav-item no-child">
      <a module='dashboard' href="{{ url('dashboard?menu=dashboard&child=no') }}" class="nav-link">
       <i class="icon ion-ios-home-outline"></i>
       <span>Dashboard</span>
      </a>
     </li><!-- nav-item -->

     <!--MASTER-->
     @if (session("hak_akses") == "superadmin")
        <li class="nav-item has-child-master">
            <a href="" class="nav-link with-sub">
            <i class="icon ion-ios-bookmarks-outline"></i>
            <span>Data</span>
            </a>
            <ul class="nav-sub">
                <li class="nav-item"><a module='produk_reseller' href="{{ url('data/produk_reseller?menu=produk_reseller&child=data') }}" class="nav-link">Stok Admin</a></li>
                <li class="nav-item"><a module='stok_reseller' href="{{ url('data/stok_reseller?menu=stok_reseller&child=data') }}" class="nav-link">Stok Reseller</a></li>
                <li class="nav-item"><a module='stok_keep' href="{{ url('data/stok_keep?menu=stok_keep&child=data') }}" class="nav-link">Stok Keep</a></li>
                <li class="nav-item"><a module='pembelian' href="{{ url('data/pembelian?menu=pembelian&child=data') }}" class="nav-link">Pembelian</a></li>
                <li class="nav-item"><a module='pengeluaran' href="{{ url('data/pengeluaran?menu=pengeluaran&child=data') }}" class="nav-link">Pengeluaran</a></li>
                <li class="nav-item"><a module='pemasukan' href="{{ url('data/pemasukan?menu=pemasukan&child=data') }}" class="nav-link">Pemasukan</a></li>
            </ul>
       </li>
     @endif
     @if (session("hak_akses") == "myoffice")
        <!--MYOFFICE-->
        <li class="nav-item has-child-master">
            <a href="" class="nav-link with-sub">
                <i class="icon ion-ios-bookmarks-outline"></i>
                <span>My Office</span>
            </a>
            <ul class="nav-sub">
                <li class="nav-item"><a module='pengeluaran' href="{{ url('myoffice/pengeluaran?menu=pengeluaran&child=myoffice') }}" class="nav-link">Pengeluaran</a></li>
            </ul>
        </li>
     @endif

     <li class="nav-item no-child">
        <a module='login' href="{{ url('login/sign_out') }}" class="nav-link">
         <i class="icon ion-ios-user"></i>
         <span>Sign Out</span>
        </a>
       </li><!-- nav-item -->
    </ul>
   </div><!-- sh-sideleft-menu -->


   <script type="text/javascript">
        Template.generateSidebar();
    </script>
