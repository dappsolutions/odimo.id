<div class="sh-sideleft-menu">
    <label class="sh-sidebar-label">Navigation</label>
    <ul class="nav">
     <li class="nav-item no-child">
      <a module='dashboard' href="{{ url('dashboard?menu=dashboard&child=no') }}" class="nav-link">
       <i class="icon ion-ios-home-outline"></i>
       <span>Dashboard</span>
      </a>
     </li><!-- nav-item -->

     <!--MASTER-->
     <li class="nav-item has-child-master">
      <a href="" class="nav-link with-sub">
       <i class="icon ion-ios-bookmarks-outline"></i>
       <span>Master</span>
      </a>
      <ul class="nav-sub">
       <li class="nav-item"><a module='guru' href="{{ url('master/guru?menu=guru&child=master') }}" class="nav-link">Guru</a></li>
       <li class="nav-item"><a module='siswa' href="{{ url('master/siswa?menu=siswa&child=master') }}" class="nav-link">Siswa</a></li>
       <li class="nav-item"><a module='angkatan' href="{{ url('master/angkatan?menu=angkatan&child=master') }}" class="nav-link">Angkatan</a></li>
       <li class="nav-item"><a module='jurusan' href="{{ url('master/jurusan?menu=jurusan&child=master') }}" class="nav-link">Jurusan</a></li>
       <li class="nav-item"><a module='kelas' href="{{ url('master/kelas?menu=kelas&child=master') }}" class="nav-link">Kelas</a></li>
       <li class="nav-item"><a module='mapel' href="{{ url('master/mapel?menu=mapel&child=master') }}" class="nav-link">Mata Pelajaran</a></li>
      </ul>
     </li>

     <!--Mentoring-->
     <li class="nav-item has-child-mentoring">
      <a href="" class="nav-link with-sub">
       <i class="icon ion-clipboard"></i>
       <span>Mentoring</span>
      </a>
      <ul class="nav-sub">
       <li class="nav-item"><a module='kategori_soal' href="{{ url('mentoring/kategori_soal?menu=kategori_soal&child=mentoring') }}" class="nav-link">Kategori Soal</a></li>
       <li class="nav-item"><a module='bank_soal' href="{{ url('mentoring/bank_soal?menu=bank_soal&child=mentoring') }}" class="nav-link">Bank Soal</a></li>
      </ul>
     </li>

     <!--Lesson-->
     <li class="nav-item has-child-lesson">
      <a href="" class="nav-link with-sub">
       <i class="icon ion-clipboard"></i>
       <span>Lesson</span>
      </a>
      <ul class="nav-sub">
       <li class="nav-item"><a module='jam_pelajaran' href="{{ url('lesson/jam_pelajaran?menu=jam_pelajaran&child=lesson') }}" class="nav-link">Jam Pelajaran</a></li>
      </ul>
     </li>

     <!--Quiz-->
     <li class="nav-item has-child-quiz">
      <a href="" class="nav-link with-sub">
       <i class="icon ion-clipboard"></i>
       <span>Quiz</span>
      </a>
      <ul class="nav-sub">
       <li class="nav-item"><a module='batas_waktu' href="{{ url('quiz/batas_waktu?menu=batas_waktu&child=quiz') }}" class="nav-link">Batas Waktu Quiz</a></li>
       <li class="nav-item"><a module='posting_quiz' href="{{ url('quiz/posting_quiz?menu=posting_quiz&child=quiz') }}" class="nav-link">Post Quiz</a></li>
      </ul>
     </li>
    </ul>
   </div><!-- sh-sideleft-menu -->


   <script type="text/javascript">
        Template.generateSidebar();
    </script>
