
<div class='row'>
    <div class="col-md-3">
        <h5>Total : Rp, <b>{{ $total_pengeluaran }}</b></h5>
    </div>
    <div class="col-md-9">
        <div class="input-group">
            <input type="search" class="form-control" placeholder="Search" onkeyup="Pengeluaran.cari(this, event)">
            <span class="input-group-btn">
                <button class="btn"><i class="fa fa-search"></i></button>
            </span><!-- input-group-btn -->
        </div>
    </div>
</div>


@if (isset($keyword))
    <div class="row">
        <div class="col-md-12">
            Hasil Pencarian : <b>{{ $keyword }}</b>
        </div>
    </div>
    <br>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">{{ 'DAFTAR '.strtoupper($module) }}</div>
            <div class="card-body">
                <div class='table-responsive'>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Keterangan</th>
                                <th>Jumlah</th>
                                <th>Tanggal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($data))
                                @php
                                    $no = ($data->currentpage()-1)* $data->perpage();
                                    $no +=1;
                                @endphp

                                @foreach ($data as $value)
                                    <tr id='{{ $value->id }}'>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $value->keterangan }}</td>
                                        <td>{{ number_format($value->total) }}</td>
                                        <td>{{ $value->createddate }}</td>
                                        <!--<td class='text-center'>-->
                                            <!--<a href=""  onclick="Pengeluaran.edit(this, event)" class="btn btn-outline-success btn-icon rounded-circle"><div><i class="fa fa-pencil"></i></div></a>-->
                                            <!--<a href="" onclick="Pengeluaran.hapus(this, event)" class="btn btn-outline-danger btn-icon rounded-circle mg-r-5"><div><i class="fa fa-trash"></i></div></a>-->
                                            <!--<a href="" onclick="Pengeluaran.detail(this, event)" class="btn btn-outline-warning btn-icon rounded-circle mg-r-5"><div><i class="fa fa-file-text"></i></div></a>-->
                                        <!--</td>-->
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">Tidak ada data ditemukan</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="card-footer">
                <div class="text-right">
                    @php
                        $no_hal = ($data->currentpage()-1)* $data->perpage();
                    @endphp
                    <span>Menampilkan data {{ $no_hal >= 0 ? $data->total() > 0 ? $no_hal+ 1 : 0 : 0 }} - {{ $no-1 }} dari <b>{{ $data->total() }}</b> data.</span>
                </div>
            </div>
        </div>
    </div>
</div>
<br>

<div class="row ">
    <div class="col-md-12">
        <div class="text-right">
            {{ $data->links() }}
        </div>
    </div>
</div>
