<input type="hidden" id='id' value="{{ isset($id) ? $id : '' }}">

<div class="row">
    <div class="col-md-12">
        <div class="card bd-primary mg-t-20">
            <div class="card-header bg-primary tx-white">{{ 'DETAIL '.strtoupper($module) }}</div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-layout">
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Kategori Produk : <span class="tx-danger">*</span></label>
                                        <input readonly id='kategori' class="form-control required" error="Kategori" value="{{ isset($kategori) ? $kategori : '' }}" type="text" placeholder="Kategori">
                                    </div>
                                </div><!-- col-4 -->

                            </div><!-- row -->
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Deskripsi Produk : <span class="tx-danger">*</span></label>
                                        <input readonly id='kategori' class="form-control required" error="Kategori" value="{{ isset($deskripsi) ? $deskripsi : '' }}" type="text" placeholder="Kategori">
                                    </div>
                                </div><!-- col-4 -->

                            </div><!-- row -->
                             <div class="row">

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Stok Produk : <span class="tx-danger">*</span></label>
                                        <input readonly id='kategori' class="form-control required" error="Kategori" value="{{ isset($stok) ? $stok : '' }}" type="text" placeholder="Kategori">
                                    </div>
                                </div><!-- col-4 -->

                            </div><!-- row -->
                            <div class="row">
                                 <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Nama Produk : <span class="tx-danger">*</span></label>
                                        <input readonly id='kategori' class="form-control required" error="Kategori" value="{{ isset($nama_product) ? $nama_product : '' }}" type="text" placeholder="Kategori">
                                    </div>
                                </div><!-- col-4 -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                         <div class="row">
                                
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label class="form-control-label">Gambar Produk : <span class="tx-danger">*</span></label>
                                        <br/>
                                        <img src="{{ $foto }}"/>
                                    </div>
                                </div><!-- col-4 -->

                            </div><!-- row -->
                    </div>
                </div>
                <hr>
            </div>

            <div class="card-footer">
                <div class="text-right">
                    <button class="btn btn-secondary" onclick="ProdukReseller.cancel()">Kembali</button>
                </div>
            </div><!-- form-layout-footer -->
        </div>
    </div>
</div>
<br>
