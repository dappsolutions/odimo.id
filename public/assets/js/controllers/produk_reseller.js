var ProdukReseller = {
    module: function () {
        return "data/produk_reseller";
    },

    link_get_module_url: function () {
        return "menu=produk_reseller&child=data";
    },

    ajaxSetup: function () {
        console.log('tes ', $('meta[name="csrf_token"]').attr('content'));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
    },

    add: function (elm, e) {
        e.preventDefault();
        window.location.href = url.base_url_with_slash(ProdukReseller.module()) + "add?"+ProdukReseller.link_get_module_url();
    },

    edit: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        window.location.href = url.base_url_with_slash(ProdukReseller.module()) + "edit/" + id+"?"+ProdukReseller.link_get_module_url();
    },

    detail: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        window.open(url.base_url_with_slash(ProdukReseller.module()) + "detail/" + id+"?"+ProdukReseller.link_get_module_url());
    },

    cancel: function () {
        window.location.href = url.base_url_with_slash(ProdukReseller.module())+"?"+ProdukReseller.link_get_module_url();
    },

    cari: function (elm, e) {
        if (e.keyCode == 13) {
            var keyword = $(elm).val();
            if (keyword == '') {
                window.location.href = url.base_url_with_slash(ProdukReseller.module())+"?"+ProdukReseller.link_get_module_url();
            } else {
                window.location.href = url.base_url_with_slash(ProdukReseller.module()) + "cari?keyword=" + $.trim(keyword)+"&"+ ProdukReseller.link_get_module_url();
            }
        }
    },

    hapus: function (elm, e) {
        e.preventDefault();
        var id = $(elm).closest('tr').attr('id');
        var html = '<div class="modal-content bd-0 tx-14">';
        // html += '<div class="modal-header pd-y-20 pd-x-25">';
        // html += '<h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message Preview</h6>';
        // html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
        // html += '<span aria-hidden="true">×</span>';
        // html += '</button>';
        // html += '</div>';
        html += '<div class="modal-body pd-25">';
        html += '<h4 class="lh-3 mg-b-20"><a href="" class="tx-inverse hover-primary">Informasi</a></h4>';
        html += '<p class="mg-b-5">Apa anda yakin akan menghapus data ini ?</p>';
        html += '</div>';
        html += '<div class="text-right">';
        html += '<button type="button" data_id="'+id+'" onclick="ProdukReseller.delete(this)" class="btn btn-success pd-x-20">Ya</button>&nbsp;';
        html += '<button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Tidak</button>';
        html += '</div>';
        html += '</div>';

        bootbox.dialog({
            message: html,
            size: 'large'
        });
    },

    delete: function(elm){
        var id = $(elm).attr('data_id');
        ProdukReseller.ajaxSetup();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {
                id: id
            },
            url: url.base_url_with_slash(ProdukReseller.module()) + 'delete',
            error: function () {
                toastr.error('Data Gagal Hapus, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Hapus..");
            },

            success: function (resp) {
                message.hideLoading();
                if (resp.is_valid) {
                    toastr.success('Data Berhasil Hapus', 'Pesan');
                    var reload = function () {
                        window.location.reload();
                    };

                    setTimeout(reload(), 1000);
                } else {
                    toastr.error('Data Gagal Hapus', 'Pesan');
                }
            }
        });
    },

    getPostInput: function () {
        var data = {
            'id': $('#id').val(),
            'nama_product': $('#nama_product').val(),
            'deskripsi': $('#deskripsi').val(),
            'stok': $('#stok').val(),
            'kategori': $('#kategori').val(),
            'encodeimage': $('#source-doc').attr('src'),
        };

        return data;
    },

    submit: function () {
        if (validation.run()) {
            var data = ProdukReseller.getPostInput();
            ProdukReseller.ajaxSetup();

            var formData = new FormData();
            formData.append('data', JSON.stringify(data));

            $.ajax({
                type: 'POST',
                dataType: 'json',
                // async: false,
                data: formData,
                processData: false,
                contentType: false,
                url: url.base_url_with_slash(ProdukReseller.module()) + 'submit',
                error: function () {
                    toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Simpan");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Disimpan', 'Pesan');
                        ProdukReseller.cancel();
                        // var reload = function () {
                        //     window.location.href = url.base_url_with_slash(StokReseller.module()) + "detail/"+resp.id+"?"+StokReseller.link_get_module_url();
                        // };

                        // setTimeout(reload(), 1000);
                    } else {
                        toastr.error('Data Gagal Disimpan', 'Pesan');
                    }
                }
            });
        }
    },


    editStok: function(elm, e){
        e.preventDefault();
        ProdukReseller.ajaxSetup();
        var params = {};
        params.id = $(elm).closest('tr').attr('id');
        $.ajax({
            type: 'POST',
            dataType: 'html',
            data: params,
            url: url.base_url_with_slash(ProdukReseller.module()) + 'editStok',
            error: function () {
                toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data...");
            },

            success: function (resp) {
                message.hideLoading();
                bootbox.dialog({
                    message: resp
                });
            }
        });
    },

    prosesStok: function(elm, e){
        e.preventDefault();
        if(validation.run()){
            ProdukReseller.ajaxSetup();
            var params = {};
            params.id = $('#produk-stok-id').val();
            params.stok = $('#stok').val();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: params,
                url: url.base_url_with_slash(ProdukReseller.module()) + 'simpanStok',
                error: function () {
                    toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Retrieving Data...");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Disimpan', 'Pesan');
                        var reload = function () {
                            window.location.reload();
                        };

                        setTimeout(reload(), 1000);
                    } else {
                        toastr.error('Data Gagal Disimpan', 'Pesan');
                    }
                }
            });
        }
    },

    keepStok: function(elm, e){
        e.preventDefault();
        ProdukReseller.ajaxSetup();
        var params = {};
        params.id = $(elm).closest('tr').attr('id');
        params.stok_sisa = $(elm).closest('tr').find('td:eq(2)').text();

        $.ajax({
            type: 'POST',
            dataType: 'html',
            data: params,
            url: url.base_url_with_slash(ProdukReseller.module()) + 'keepStok',
            error: function () {
                toastr.error('Data Gagal Data Timeout', 'Pesan');
                message.hideLoading();
            },

            beforeSend: function () {
                message.loading("Sedang Proses Retrieving Data...");
            },

            success: function (resp) {
                message.hideLoading();
                bootbox.dialog({
                    message: resp
                });
            }
        });
    },

    prosesKeepStok: function(elm, e){
        e.preventDefault();
        if(validation.run()){
            ProdukReseller.ajaxSetup();
            var params = {};
            params.id = $('#produk-stok-id').val();
            params.customer = $('#nama-customer').val();
            params.stok_sisa = $('#sisa-stok').val();
            params.deskripsi = $('#deskripsi').val();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: params,
                url: url.base_url_with_slash(ProdukReseller.module()) + 'prosesKeepStok',
                error: function () {
                    toastr.error('Data Gagal Disimpan, Data Timeout', 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Sedang Proses Retrieving Data...");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success('Data Berhasil Disimpan', 'Pesan');
                        var reload = function () {
                            window.location.reload();
                        };

                        setTimeout(reload(), 1000);
                    } else {
                        toastr.error('Data Gagal Disimpan', 'Pesan');
                    }
                }
            });
        }
    },

    inputFile: function (elm) {
        var uploader = $('<input type="file" accept="*" />');
        var src_doc = $("#source-doc");
        uploader.click();

        uploader.on("change", function () {
          var reader = new FileReader();
          reader.onload = function (event) {
            var files = $(uploader).get(0).files[0];
            filename = files.name;
            var data_from_file = filename.split(".");
            var type_file = $.trim(data_from_file[data_from_file.length - 1]);

            var data = event.target.result;
            src_doc.attr("src", data);

            // var data_source = data.split(';');
            // console.log(data_source[1].length);
            src_doc.removeClass("hide");
          };

          reader.readAsDataURL(uploader[0].files[0]);
        });
    },

};


$(function () {

});
