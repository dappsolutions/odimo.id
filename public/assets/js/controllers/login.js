var Login = {
    module: function () {
        return 'login';
    },

    ajaxSetup: function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
            }
        });
    },

    sign_in: function (elm, e) {
        e.preventDefault();

        var username = $('#username').val();
        var password = $('#password').val();

        //   message.loading("Proses Authentifikasi Data...");
        //   return;
        if (validation.run()) {
            Login.ajaxSetup();
            $.ajax({
                type: 'POST',
                data: {
                    username: username,
                    password: password
                },
                dataType: 'json',
                // async: false,
                url: url.base_url_with_slash(Login.module()) + 'sign_in',
                error: function () {
                    toastr.error("Gagal", 'Pesan');
                    message.hideLoading();
                },

                beforeSend: function () {
                    message.loading("Proses Authentifikasi Data...");
                },

                success: function (resp) {
                    message.hideLoading();
                    if (resp.is_valid) {
                        toastr.success("Login Berhasil", 'Pesan');
                        if (resp.hak_akses == 'Kasir') {
                            setTimeout(Login.goto_pembelian(), 1000);
                        } else {
                            setTimeout(Login.goto_dashboard(), 1000);
                        }
                    } else {
                        toastr.error("Username atau Password Tidak Valid", 'Pesan');
                    }
                }
            });
        }
    },

    goto_dashboard: function () {
        // var url = "dashboard/dashboard";
        window.location.href = url.base_url('dashboard')+"?menu=dashboard&child=no";
    },
};

$(function () {

});
