<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class User
{

    public function getTableUser()
    {
        return 'user';
    }

    public static function getSekolahId()
    {
        $tb_user = 'users';
        $tb_guru = 'guru';
        $tb_siswa = 'siswa';
        $tb_sekolah = 'sekolah';
        $tb_guru_has_user = 'guru_has_user';
        $tb_siswa_has_user = 'siswa_has_user';
        $tb_sekolah_has_user = 'sekolah_has_user';

        $user = session('user_id');
        $data = DB::table($tb_user)
            ->select(
                $tb_user . '.*',
                $tb_sekolah . '.id as sekolah',
                'tb_skl.id as sekolah_dua',
                $tb_sekolah_has_user . '.sekolah as sekolah_tiga'
            )
            ->leftJoin($tb_guru_has_user, $tb_user . '.id', '=', $tb_guru_has_user . '.users')
            ->leftJoin($tb_guru, $tb_guru_has_user . '.guru', '=', $tb_guru . '.id')
            ->leftJoin($tb_sekolah, $tb_guru . '.sekolah', '=', $tb_sekolah . '.id')
            ->leftJoin($tb_siswa_has_user, $tb_user . '.id', '=', $tb_siswa_has_user . '.users')
            ->leftJoin($tb_siswa, $tb_siswa_has_user . '.siswa', '=', $tb_siswa . '.id')
            ->leftJoin($tb_sekolah . ' as tb_skl', $tb_siswa . '.sekolah', '=', 'tb_skl.id')
            ->leftJoin($tb_sekolah_has_user, $tb_user . '.id', '=', $tb_sekolah_has_user . '.users')
            ->get();


        // echo '<pre>';
        // print_r($data->toArray());
        // die;
        return $user;
    }
}
