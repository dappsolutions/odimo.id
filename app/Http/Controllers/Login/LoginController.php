<?php

namespace App\Http\Controllers\Login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public $tb_user = "user";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function index()
    {
        $content['token'] = csrf_token();
        return view("login.login", $content);
    }

    public function sign_out()
    {
        session()->flush();
        return redirect()->to('/')->with('message', 'Anda telah keluar dari aplikasi')->send();
    }

    public function sign_in(Request $req)
    {

        session()->flush();
        $data = DB::table($this->tb_user . ' as us')
            ->select(
                'us.*'
            )
            ->where('us.username', '=', $req->username)
            ->where('us.password', '=', $req->password)
            ->get();

        $is_valid = false;
        if (!empty($data->toArray())) {
            $is_valid = true;
            $data = $data->first();

            session(array(
                'user_id' => $data->id,
                'username' => $data->username,
                'hak_akses' => $data->hak_akses
            ));
        }
        return json_encode(array(
            'is_valid' => $is_valid
        ));
    }

    public function getSekolahId($data)
    {
        $sekolah_id = '';
        if ($data->sekolah != '') {
            $sekolah_id = $data->sekolah;
        }

        if ($data->sekolah_dua != '') {
            $sekolah_id = $data->sekolah_dua;
        }

        if ($data->sekolah_tiga != '') {
            $sekolah_id = $data->sekolah_tiga;
        }

        return $sekolah_id;
    }

    public function getData()
    {
        $data = DB::table('users')->get();
        $data = $data->toArray();
        echo json_encode(array(
            'data' => $data
        ));
    }
}
