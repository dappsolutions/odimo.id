<?php

namespace App\Http\Controllers\Pembelian;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Session;

class PengeluaranController extends Controller
{
    public $module = "pengeluaran";

    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {
        echo 'oke';
    }

    public function simpanTransksi(Request $req)
    {
        $is_valid = false;
        $message = "";
        try {
            $push['keterangan'] = $req['keterangan'];
            $push['total'] = $req['jumlah'];
            $push['createddate'] = date('Y-m-d H:i:s');

            DB::table('pengeluaran')->insert($push);
            $is_valid = true;
            $message = "Berhasil di Simpan";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message
            )
        );
    }
}
