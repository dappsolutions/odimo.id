<?php

namespace App\Http\Controllers\Pembelian;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Transactions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
// use Session;

class ProdukStokController extends Controller
{
    public $keyword;
    public function index(){
        $summary_qty = DB::table('product_stock as ps')
        ->selectRaw('sum(cs.stok) as total')
        ->joinSub("select max(id) as id, product_stock from change_stok group by product_stock", "stok_max", "stok_max.product_stock", "=", 'ps.id')
            ->join('change_stok as cs', function ($join) {
                $join->on('cs.id', '=', 'stok_max.id');
        })
        ->where(function ($query) {
                $query->Where('ps.kategori', 'like', '%' . $this->keyword . '%');
        })
        ->get();  
        
        echo '<pre>';
        print_r($summary_qty->toArray());
    }
    
    public function getDataProdukStok(Request $req)
    {
        $this->keyword =  $req['keyword'];
         $summary_qty = DB::table('product_stock as ps')
        ->selectRaw('sum(cs.stok) as total')
        ->joinSub("select max(id) as id, product_stock from change_stok group by product_stock", "stok_max", "stok_max.product_stock", "=", 'ps.id')
            ->join('change_stok as cs', function ($join) {
                $join->on('cs.id', '=', 'stok_max.id');
        })
        ->where(function ($query) {
                $query->Where('ps.kategori', 'like', '%' . $this->keyword . '%');
        })
        ->whereNull('ps.closestok')
        ->where('ps.tipe', '=', 'RESELLER')
        ->get();  
        
        // echo $this->keyword;die;
        $data = DB::table("product_stock as ps")
        ->select('ps.*', 'cs.stok as stok_valid')
        ->joinSub("select max(id) as id, product_stock from change_stok group by product_stock", "stok_max", "stok_max.product_stock", "=", 'ps.id')
            ->join('change_stok as cs', function ($join) {
                $join->on('cs.id', '=', 'stok_max.id');
        })
        ->where(function ($query) {
                $query->Where('ps.kategori', 'like', '%' . $this->keyword . '%');
        })
        ->whereNull('ps.closestok')
        ->where('ps.tipe', '=', 'RESELLER')
        ->orderBy('ps.id', 'desc')
        ->paginate(50);

        // echo '<pre>';
        // print_r($data->toArray());die;

        // $total = 0;
        $result = array();
        $next_page = intval($data->toArray()['current_page']) < intval($data->toArray()['last_page']) ? $data->toArray()['current_page']+1 : 'last';
        
        if(!empty($data->toArray())){
            foreach($data->toArray()['data'] as $value){
                // $total += $value->stok_valid;
                $value->stok = $value->stok_valid;
                array_push($result, $value);
            }

            
        }

        echo json_encode(array(
            'data' => $result,
            'next_page'=> $next_page,
            'total_stok'=> $summary_qty->toArray()[0]->total,
        ));
    }
    
     public function getDataProdukStokAdmin(Request $req)
    {
        $this->keyword =  $req['keyword'];
         $summary_qty = DB::table('product_stock as ps')
        ->selectRaw('sum(cs.stok) as total')
        ->joinSub("select max(id) as id, product_stock from change_stok group by product_stock", "stok_max", "stok_max.product_stock", "=", 'ps.id')
            ->join('change_stok as cs', function ($join) {
                $join->on('cs.id', '=', 'stok_max.id');
        })
        ->where(function ($query) {
                $query->Where('ps.kategori', 'like', '%' . $this->keyword . '%');
        })
        ->whereNull('ps.closestok')
        ->whereNull('ps.tipe')
        ->get();  
        
        // echo $this->keyword;die;
        $data = DB::table("product_stock as ps")
        ->select('ps.*', 'cs.stok as stok_valid')
        ->joinSub("select max(id) as id, product_stock from change_stok group by product_stock", "stok_max", "stok_max.product_stock", "=", 'ps.id')
            ->join('change_stok as cs', function ($join) {
                $join->on('cs.id', '=', 'stok_max.id');
        })
        ->where(function ($query) {
                $query->Where('ps.kategori', 'like', '%' . $this->keyword . '%');
        })
        ->whereNull('ps.closestok')
        ->whereNull('ps.tipe')
        ->orderBy('ps.id', 'desc')
        ->paginate(50);

        // echo '<pre>';
        // print_r($data->toArray());die;

        // $total = 0;
        $result = array();
        $next_page = intval($data->toArray()['current_page']) < intval($data->toArray()['last_page']) ? $data->toArray()['current_page']+1 : 'last';
        
        if(!empty($data->toArray())){
            foreach($data->toArray()['data'] as $value){
                // $total += $value->stok_valid;
                $value->stok = $value->stok_valid;
                array_push($result, $value);
            }

            
        }

        echo json_encode(array(
            'data' => $result,
            'next_page'=> $next_page,
            'total_stok'=> $summary_qty->toArray()[0]->total,
        ));
    }

    public function getDataProdukStokPost(Request $req)
    {
        $filter = $req['filter_kategori'];
        
        $data = DB::table("product_stock as ps")
        ->select('ps.*')
        ->orderBy('ps.id', 'desc')
        ->paginate(50);

        // echo '<pre>';
        // print_r($data->toArray());die;

        $total = 0;
        $result = array();
        $next_page = intval($data->toArray()['current_page']) < intval($data->toArray()['last_page']) ? $data->toArray()['current_page']+1 : 'last';
        
        if(!empty($data->toArray())){
            foreach($data->toArray()['data'] as $value){
                $total += $value->stok;
                array_push($result, $value);
            }

            
        }

        echo json_encode(array(
            'data' => $result,
            'next_page'=> $next_page,
            'total_stok'=> $total,
        ));
    }
    
    public function simpanTransksi(Request $req)
    {
        $is_valid = false;
        $message = "";
        try {
            $push['nama_product'] = $req['nama_product'];
            $push['foto'] = $req['encodeimages'];
            $push['stok'] = $req['stok'];
            $push['kategori'] = $req['kategori'];
            $push['deskripsi'] = $req['deskripsi'];
            $push['createddate'] = date('Y-m-d H:i:s');
            
            if(isset($req['tipe'])){
                if($req['tipe'] == 'RESELLER'){
                    $push['tipe'] = $req['tipe'];
                }        
            }

            $product_stok = DB::table('product_stock')->insertGetId($push);
            
            $push = array();
            $push['product_stock'] = $product_stok;
            $push['stok'] = $req['stok'];
            $push['createddate'] = date('Y-m-d H:i:s');
            DB::table('change_stok')->insert($push);
            
            $is_valid = true;
            $message = "Berhasil di Simpan";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message
            )
        );
    }
    
    public function getDataProdukResellerByNamaProduct($product_stok_id){
         $data_admin = DB::table("product_stock as ps")
        ->select('ps.*', 'cs.stok as stok_valid')
        ->joinSub("select max(id) as id, product_stock from change_stok group by product_stock", "stok_max", "stok_max.product_stock", "=", 'ps.id')
            ->join('change_stok as cs', function ($join) {
                $join->on('cs.id', '=', 'stok_max.id');
        })
        ->where('ps.id', '=', $product_stok_id)
        ->whereNull('ps.closestok')
        ->whereNull('ps.tipe')
        ->orderBy('ps.id', 'desc')
        ->get();
        
        $product_id = '';
        if(!empty($data_admin->toArray())){
            $data_admin = $data_admin->first();
            // echo '<pre>';
            // print_r($data_admin);die;
            $data = DB::table("product_stock as ps")
            ->select('ps.*', 'cs.stok as stok_valid')
            ->joinSub("select max(id) as id, product_stock from change_stok group by product_stock", "stok_max", "stok_max.product_stock", "=", 'ps.id')
                ->join('change_stok as cs', function ($join) {
                    $join->on('cs.id', '=', 'stok_max.id');
            })
            ->where('ps.nama_product', '=', $data_admin->nama_product)
            ->where('ps.tipe', '=', 'RESELLER')
            ->whereNull('ps.closestok')
            ->orderBy('ps.id', 'desc')
            ->get();
            
            if(!empty($data->toArray())){
                $data = $data->first();
                $product_id = $data->id;
            }
        }
        
        return $product_id;
    }
    
    public function simpanStok(Request $req)
    {
        $is_valid = false;
        $message = "";
        try {
            $push = array();
            $push['product_stock'] = $req['product_stok_id'];
            $push['stok'] = $req['stok'];
            $push['createddate'] = date('Y-m-d H:i:s');
            DB::table('change_stok')->insert($push);
            
            
            $product_id = $this->getDataProdukResellerByNamaProduct($req['product_stok_id']);
            
            if($product_id != ''){
                 $push = array();
                $push['product_stock'] = $product_id;
                $push['stok'] = $req['stok'];
                $push['createddate'] = date('Y-m-d H:i:s');
                DB::table('change_stok')->insert($push);
            }
            
            $is_valid = true;
            $message = "Berhasil di Simpan";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message
            )
        );
    }
}
