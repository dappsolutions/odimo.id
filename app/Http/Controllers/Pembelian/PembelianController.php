<?php

namespace App\Http\Controllers\Pembelian;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Transactions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
// use Session;

class PembelianController extends Controller
{
    public $module = "dashboard";

    public function getHeaderCss()
    {
        return array(
            'js' => asset('assets/js/url.js'),
            'js' => asset('assets/js/message.js'),
            'js' => asset('assets/js/controllers/dashboard.js'),
        );
    }

    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index(Request $req)
    {
        $data['username'] = 'Dodik Rismawan';
        $data['title_top'] = 'Main';

        echo '<pre>';
        print_r($data);
        die;
        $content['data'] = "DASHBOARD";
        $view = view("dashboard.dashboard", $content);

        $data['view_file'] = $view;
        $data['title_content'] = 'Dashboard';
        $data['module'] = $this->module;
        $data['header_data'] = $this->getHeaderCss();
        return view("template.main", $data);
    }

    public function getDataProduk()
    {
        $summary = DB::table('transactions')->selectRaw('sum(harga*qty) as total')->whereNull('closeorder')->get();        
        $summary_qty = DB::table('transactions')->selectRaw('sum(qty) as total')->whereNull('closeorder')->get();        
        $data = DB::table("transactions as ts")
        ->select('ts.*', 'tt.tipe')
        ->join('type_transactions as tt', 'tt.id', '=', 'ts.type_transactions')
        ->whereNull('ts.closeorder')
        ->orderBy('ts.id', 'desc')
        ->paginate(50);

        // echo '<pre>';
        // print_r($data->toArray());die;

        $result = array();
        $next_page = intval($data->toArray()['current_page']) < intval($data->toArray()['last_page']) ? $data->toArray()['current_page']+1 : 'last';
        
        if(!empty($data->toArray())){
            foreach($data->toArray()['data'] as $value){
                $value->harga = number_format($value->harga);
                array_push($result, $value);
            }

            
        }

        echo json_encode(array(
            'data' => $result,
            'next_page'=> $next_page,
            'total_pembelian'=> number_format($summary->toArray()[0]->total),
            // 'total_pembelian'=> '-',
            'barang_terjual'=> $summary_qty->toArray()[0]->total
        ));
    }
    
    public function getDataProdukForAdmin()
    {
        $summary = DB::table('transactions')->selectRaw('sum(harga*qty) as total')->whereNull('closeorder')->get();        
        $summary_qty = DB::table('transactions')->selectRaw('sum(qty) as total')->whereNull('closeorder')->get();        
        $data = DB::table("transactions as ts")
        ->select('ts.*', 'tt.tipe')
        ->join('type_transactions as tt', 'tt.id', '=', 'ts.type_transactions')
        ->whereNull('ts.closeorder')
        ->orderBy('ts.id', 'desc')
        ->paginate(50);

        // echo '<pre>';
        // print_r($data->toArray());die;

        $result = array();
        $next_page = intval($data->toArray()['current_page']) < intval($data->toArray()['last_page']) ? $data->toArray()['current_page']+1 : 'last';
        
        if(!empty($data->toArray())){
            foreach($data->toArray()['data'] as $value){
                $value->harga = number_format($value->harga);
                array_push($result, $value);
            }

            
        }

        echo json_encode(array(
            'data' => $result,
            'next_page'=> $next_page,
            //  'total_pembelian'=> number_format($summary->toArray()[0]->total),
             'total_pembelian'=> '-',
            'barang_terjual'=> $summary_qty->toArray()[0]->total
        ));
    }

    public function simpanTransksi(Request $req)
    {
        $is_valid = false;
        $message = "";
        try {
            $type_trans = $req['type_trans'];
            switch ($type_trans) {
                case 'CASH':
                    $type_trans = "1";
                    break;
                case 'HUTANG':
                    $type_trans = "2";
                    break;
                case 'TRANSFER':
                    $type_trans = "3";
                    break;

                default:
                    break;
            }
            $push['nama_product'] = $req['nama_barang'];
            $push['harga'] = $req['harga'];
            $push['type_transactions'] = $type_trans;
            $push['qty'] = $req['qty'];
            $push['createddate'] = date('Y-m-d H:i:s');

            DB::table('transactions')->insert($push);
            $is_valid = true;
            $message = "Berhasil di Simpan";
        } catch (\Throwable $th) {
            $is_valid = false;
            $message = $th->getMessage();
        }

        echo json_encode(
            array(
                'is_valid' => $is_valid,
                'message' => $message
            )
        );
    }
}
