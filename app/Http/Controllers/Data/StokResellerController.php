<?php

namespace App\Http\Controllers\Data;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\ProductStock;

class StokResellerController extends Controller
{
    public $limit = 5;
    public $tb = "product_stock";
    public $keyword = "";
    public $link_get_url = "menu=stok_reseller&child=data";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/url.js'),
            'js-2' => asset('assets/js/message.js'),
            'js-3' => asset('assets/js/validation.js'),
            'js-4' => asset('assets/js/controllers/stok_reseller.js'),
        );
    }

    public function getModuleName()
    {
        return "stok_reseller";
    }

    public function index(Request $req)
    {
        $data = $this->getListData('?' . $this->link_get_url);

        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $view = view("stok_reseller.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Produk Reseller';
        $dataput['title_top'] = 'Produk Reseller ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function add()
    {
        $content['module'] = $this->getModuleName();
        $view = view("stok_reseller.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Produk ';
        $dataput['title_top'] = 'Produk ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function edit($id)
    {
        $data = ProductStock::where('id', '=', $id)->first();

        $content = $data->toArray();
        $content['module'] = $this->getModuleName();
        $view = view("stok_reseller.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Produk ';
        $dataput['title_top'] = 'Produk ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function detail($id)
    {
        $data = ProductStock::where('id', '=', $id)->first();
        $content = $data->toArray();
        $content['foto'] = "data:image/png;base64,".$content['foto'];
        $content['module'] = $this->getModuleName();
        $view = view("stok_reseller.detaildata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Produk ';
        $dataput['title_top'] = 'Produk ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListData($with_path = "")
    {
        $data = DB::table($this->tb)
            ->select($this->tb . ".*", "cs.stok as stok_valid")
            ->joinSub("select max(id) as id, product_stock from change_stok group by product_stock", "stok_max", "stok_max.product_stock", "=", $this->tb.'.id')
            ->join('change_stok as cs', function ($join) {
                $join->on('cs.id', '=', 'stok_max.id');
            })
            ->where(function ($query) {
                $query->where($this->tb . '.deleted', '=', '0');
            })->where(function ($query) {
                $query->Where($this->tb . '.nama_product', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb . '.createddate', 'like', '%' . $this->keyword . '%');
            })
            ->whereNull($this->tb.".closestok")
            ->where($this->tb.".tipe", "=", "RESELLER")
            ->orderBy($this->tb . '.id', 'desc')
            ->paginate($this->limit);


        if ($with_path != '') {
            $data->withPath($with_path);
        }

        return $data;
    }

    public function cari(Request $req)
    {
        $this->keyword = trim($req->keyword);
        $data = $this->getListData('cari?keyword=' . $this->keyword . '&' . $this->link_get_url);
        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $content['keyword'] = $this->keyword;
        $view = view("stok_reseller.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Produk ';
        $dataput['title_top'] = 'Produk ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getPostInput($param)
    {
        $data = array();
        $data['kategori'] = $param->kategori;

        return $data;
    }

    public function submit(Request $req)
    {
        $data = json_decode($req['data']);
        $id = $data->id;
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push = array();
            $push['nama_product'] = $data->nama_product;
            
            $img = explode('base64,', $data->encodeimage);
            if(!empty($img)){
                $push['foto'] = trim($img[1]);   
            }
            $push['stok'] = $data->stok;
            $push['kategori'] = $data->kategori;
            $push['deskripsi'] = $data->deskripsi;
            $push['createddate'] = date('Y-m-d H:i:s');
            $push['tipe'] = 'RESELLER';
            $product_stok = DB::table('product_stock')->insertGetId($push);
            
            $push = array();
            $push['product_stock'] = $product_stok;
            $push['stok'] = $data->stok;
            $push['createddate'] = date('Y-m-d H:i:s');
            DB::table('change_stok')->insert($push);

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid, 'id' => $id));
    }

    public function delete(Request $req)
    {
        $id = $req['id'];
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push['deleted'] = 1;
            DB::table($this->tb)->where('id', '=', $id)->update($push);
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function getPostInputGuruHasMapel($param)
    {
        $data = array();
        $data['guru'] = $param->guru;
        $data['mata_pelajaran'] = $param->mapel_id;
        $data['handled'] = $param->checked;

        return $data;
    }

    public function changeMapel(Request $req)
    {
        $data = json_decode($req['data']);
        $is_valid = false;

        DB::beginTransaction();
        try {

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $guru_mapel_id = $value->guru_mapel_id;
                    $push = $this->getPostInputGuruHasMapel($value);
                    if ($guru_mapel_id == '') {
                        DB::table($this->tb_guru_mapel)->insert($push);
                    } else {
                        DB::table($this->tb_guru_mapel)->where('id', '=', $guru_mapel_id)->update($push);
                    }
                }
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }
}
