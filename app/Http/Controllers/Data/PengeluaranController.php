<?php

namespace App\Http\Controllers\Data;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\ProductStock;

class PengeluaranController extends Controller
{
    public $limit = 10;
    public $tb = "pengeluaran";
    public $keyword = "";
    public $link_get_url = "menu=pengeluaran&child=data";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/url.js'),
            'js-2' => asset('assets/js/message.js'),
            'js-3' => asset('assets/js/validation.js'),
            'js-4' => asset('assets/js/controllers/pengeluaran.js'),
        );
    }

    public function getModuleName()
    {
        return "pengeluaran";
    }

    public function index(Request $req)
    {
        $data = $this->getListData('?' . $this->link_get_url);
        $summary = DB::table($this->tb)->selectRaw('sum(total) as total')->whereNull('closedate')->get();


        $content['total_pengeluaran'] = number_format($summary[0]->total);
        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $view = view("pengeluaran.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Pengeluaran';
        $dataput['title_top'] = 'Pengeluaran ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function add()
    {
        $content['module'] = $this->getModuleName();
        $view = view("pengeluaran.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Produk ';
        $dataput['title_top'] = 'Produk ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function edit($id)
    {
        $data = ProductStock::where('id', '=', $id)->first();

        $content = $data->toArray();
        $content['module'] = $this->getModuleName();
        $view = view("pengeluaran.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Produk ';
        $dataput['title_top'] = 'Produk ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function detail($id)
    {
        $data = ProductStock::where('id', '=', $id)->first();
        $content = $data->toArray();
        $content['foto'] = "data:image/png;base64," . $content['foto'];
        $content['module'] = $this->getModuleName();
        $view = view("pengeluaran.detaildata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Produk ';
        $dataput['title_top'] = 'Produk ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListData($with_path = "")
    {
        $data = DB::table($this->tb)
            ->select(
                $this->tb . ".*"
            )->where(function ($query) {
                $query->Where($this->tb . '.keterangan', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb . '.total', 'like', '%' . $this->keyword . '%');
            })
            ->whereNull($this->tb . ".closedate")
            ->orderBy($this->tb . ".id", 'desc')
            ->paginate($this->limit);


        if ($with_path != '') {
            $data->withPath($with_path);
        }

        return $data;
    }

    public function cari(Request $req)
    {
        $this->keyword = trim($req->keyword);
        $data = $this->getListData('cari?keyword=' . $this->keyword . '&' . $this->link_get_url);

        $summary = DB::table($this->tb)->selectRaw('sum(total) as total')->whereNull('closedate')->get();


        $content['total_pengeluaran'] = number_format($summary[0]->total);

        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $content['keyword'] = $this->keyword;
        $view = view("pengeluaran.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Produk ';
        $dataput['title_top'] = 'Produk ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getPostInput($param)
    {
        $data = array();
        $data['kategori'] = $param->kategori;

        return $data;
    }

    public function cancelKeep(Request $req)
    {

        // echo '<pre>';
        // print_r($req->all());
        // die;
        $is_valid = false;

        DB::beginTransaction();
        try {

            //soft delete
            $push = array();
            $push['deleted'] = 1;
            DB::table('product_keep')->where("id", "=", $req['keep_id'])->update($push);

            // echo '<pre>';
            // print_r(DB::getQueryLog());
            // die;

            $push = array();
            $push['product_stock'] = $req['id'];
            $push['stok'] = intval($req['stok_sisa']) + 1;
            $push['createddate'] = date('Y-m-d H:i:s');
            DB::table('change_stok')->insert($push);

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function confirmKeep(Request $req)
    {

        // echo '<pre>';
        // print_r($req->all());
        // die;
        $is_valid = false;

        DB::beginTransaction();
        try {

            //soft delete
            $push = array();
            $push['confirmdate'] = date('Y-m-d H:i:s');
            DB::table('product_keep')->where("id", "=", $req['keep_id'])->update($push);
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function delete(Request $req)
    {
        $id = $req['id'];
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push['deleted'] = 1;
            DB::table($this->tb)->where('id', '=', $id)->update($push);
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }

    public function getPostInputGuruHasMapel($param)
    {
        $data = array();
        $data['guru'] = $param->guru;
        $data['mata_pelajaran'] = $param->mapel_id;
        $data['handled'] = $param->checked;

        return $data;
    }

    public function changeMapel(Request $req)
    {
        $data = json_decode($req['data']);
        $is_valid = false;

        DB::beginTransaction();
        try {

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $guru_mapel_id = $value->guru_mapel_id;
                    $push = $this->getPostInputGuruHasMapel($value);
                    if ($guru_mapel_id == '') {
                        DB::table($this->tb_guru_mapel)->insert($push);
                    } else {
                        DB::table($this->tb_guru_mapel)->where('id', '=', $guru_mapel_id)->update($push);
                    }
                }
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }
}
