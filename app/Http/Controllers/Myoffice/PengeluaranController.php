<?php

namespace App\Http\Controllers\Myoffice;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\ProductStock;
use App\Models\Spending;

class PengeluaranController extends Controller
{
    public $limit = 10;
    public $tb = "spending";
    public $keyword = "";
    public $link_get_url = "menu=pengeluaran&child=myoffice";

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function getHeaderCss()
    {
        return array(
            'js-1' => asset('assets/js/url.js'),
            'js-2' => asset('assets/js/message.js'),
            'js-3' => asset('assets/js/validation.js'),
            'js-4' => asset('assets/js/controllers/spending.js'),
        );
    }

    public function getModuleName()
    {
        return "pengeluaran";
    }

    public function index(Request $req)
    {
        $data = $this->getListData('?' . $this->link_get_url);
        $summary = DB::table($this->tb)->selectRaw('sum(total) as total')->whereNull('closedate')->get();


        $content['total_pengeluaran'] = number_format($summary[0]->total);
        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $view = view("myoffice.pengeluaran.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Pengeluaran';
        $dataput['title_top'] = 'Pengeluaran ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function add()
    {
        $content['module'] = $this->getModuleName();
        $view = view("myoffice.pengeluaran.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Pengeluaran ';
        $dataput['title_top'] = 'Pengeluaran ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function edit($id)
    {
        $data = ProductStock::where('id', '=', $id)->first();

        $content = $data->toArray();
        $content['module'] = $this->getModuleName();
        $view = view("myoffice.pengeluaran.adddata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Pengeluaran ';
        $dataput['title_top'] = 'Pengeluaran ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function detail($id)
    {
        $data = ProductStock::where('id', '=', $id)->first();
        $content = $data->toArray();
        $content['foto'] = "data:image/png;base64," . $content['foto'];
        $content['module'] = $this->getModuleName();
        $view = view("myoffice.pengeluaran.detaildata", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Pengeluaran ';
        $dataput['title_top'] = 'Pengeluaran ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getListData($with_path = "")
    {
        $data = DB::table($this->tb)
            ->select(
                $this->tb . ".*",
                "usr.username"
            )
            ->join("user as usr", "usr.id", "=", $this->tb . ".user")
            ->where(function ($query) {
                $query->Where($this->tb . '.keterangan', 'like', '%' . $this->keyword . '%')
                    ->orWhere($this->tb . '.total', 'like', '%' . $this->keyword . '%');
            })
            ->whereNull($this->tb . ".closedate")
            ->orderBy($this->tb . ".id", 'desc')
            ->paginate($this->limit);


        if ($with_path != '') {
            $data->withPath($with_path);
        }

        return $data;
    }

    public function cari(Request $req)
    {
        $this->keyword = trim($req->keyword);
        $data = $this->getListData('cari?keyword=' . $this->keyword . '&' . $this->link_get_url);

        $summary = DB::table($this->tb)->selectRaw('sum(total) as total')->whereNull('closedate')->get();


        $content['total_pengeluaran'] = number_format($summary[0]->total);

        $content['module'] = $this->getModuleName();
        $content['data'] = $data;
        $content['keyword'] = $this->keyword;
        $view = view("myoffice.pengeluaran.index", $content);

        $dataput['view_file'] = $view;
        $dataput['title_content'] = 'Pengeluaran ';
        $dataput['title_top'] = 'Pengeluaran ';
        $dataput['module'] = $this->getModuleName();
        $dataput['header_data'] = $this->getHeaderCss();
        return view("template.main", $dataput);
    }

    public function getPostInput($param)
    {
        $data = array();
        $data['keterangan'] = $param->keterangan;
        $data['total'] = $param->total;

        return $data;
    }

    public function submit(Request $req)
    {
        $data = json_decode($req['data']);
        $id = $data->id;
        $is_valid = false;

        // echo '<pre>';
        // print_r(session()->all());
        // die;
        DB::beginTransaction();
        try {
            $push = $this->getPostInput($data);
            if ($id == '') {
                $push['user'] = session("user_id");
                $push['createddate'] = date('Y-m-d H:i:s');
                $push['createdby'] = session('user_id');
                $id = Spending::insertGetId($push);
            } else {
                $push['updateddate'] = date('Y-m-d H:i:s');
                $push['updatedby'] = session('user_id');
                DB::table($this->tb)->where('id', '=', $id)->update($push);
            }

            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid, 'id' => $id));
    }

    public function delete(Request $req)
    {
        $id = $req['id'];
        $is_valid = false;

        DB::beginTransaction();
        try {
            $push['deleted'] = 1;
            DB::table($this->tb)->where('id', '=', $id)->update($push);
            DB::commit();
            $is_valid = true;
        } catch (Exception $ex) {
            DB::rollback();
        }

        return json_encode(array('is_valid' => $is_valid));
    }
}
